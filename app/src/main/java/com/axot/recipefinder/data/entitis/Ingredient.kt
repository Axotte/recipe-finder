package com.axot.recipefinder.data.entitis

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "ingredients",
    foreignKeys = [ForeignKey(entity = Recipe::class, parentColumns = ["id"], childColumns =  ["recipeId"], onDelete = ForeignKey.CASCADE)])
data class Ingredient(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var recipeId: Long = 0,
    var name: String = "",
    var quantity: Double = 0.0,
    var unit: String = ""
)