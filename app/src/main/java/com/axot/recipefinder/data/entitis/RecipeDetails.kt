package com.axot.recipefinder.data.entitis

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class RecipeDetails {
    @Embedded
    var recipe: Recipe?= null
    @Relation(parentColumn = "id", entityColumn = "recipeId")
    var ingredients: List<Ingredient> = ArrayList()
}