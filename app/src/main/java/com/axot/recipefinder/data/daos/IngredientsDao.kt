package com.axot.recipefinder.data.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.axot.recipefinder.data.entitis.Ingredient

@Dao
interface IngredientsDao: BaseDao<Ingredient> {
    @Query("DELETE FROM ingredients WHERE recipeId=:recipeId")
    fun deleteAllFromRecipe(recipeId: Long)

    @Query("SELECT name FROM ingredients")
    fun getAllNames():LiveData<List<String>>
}