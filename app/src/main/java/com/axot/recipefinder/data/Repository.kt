package com.axot.recipefinder.data

import com.axot.recipefinder.data.daos.IngredientsDao
import com.axot.recipefinder.data.daos.RecipesDao
import com.axot.recipefinder.data.entitis.Ingredient
import com.axot.recipefinder.data.entitis.Recipe
import javax.inject.Inject

class Repository @Inject constructor(private val ingredientsDao: IngredientsDao, private val recipesDao: RecipesDao) {
    fun getAllRecipes() = recipesDao.getAllRecipes()
    fun getRecipeDetails(id: Long) = recipesDao.getRecipeDetails(id)
    fun getAllRecipeDetails() = recipesDao.getAllRecipeDetails()
    fun insertRecipe(vararg recipe: Recipe) = recipesDao.insert(*recipe)
    fun getAllNames() = ingredientsDao.getAllNames()
    fun deleteRecipe(vararg recipe: Recipe) {
        recipesDao.delete(*recipe)
    }
    fun insertIngredient(vararg ingredient: Ingredient) {
        ingredientsDao.insert(*ingredient)
    }
    fun deleteIngredient(vararg ingredient: Ingredient) {
        ingredientsDao.delete(*ingredient)
    }
    fun deleteAllFromRecipe(recipeId: Long) {
        ingredientsDao.deleteAllFromRecipe(recipeId)
    }
}