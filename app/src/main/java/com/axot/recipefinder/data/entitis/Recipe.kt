package com.axot.recipefinder.data.entitis

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "recipes")
class Recipe (
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var name: String = "",
    var image: String = "",
    var description: String = ""
)