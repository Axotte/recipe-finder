package com.axot.recipefinder.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.axot.recipefinder.data.daos.IngredientsDao
import com.axot.recipefinder.data.daos.RecipesDao
import com.axot.recipefinder.data.entitis.Ingredient
import com.axot.recipefinder.data.entitis.Recipe

@Database(entities = [Recipe::class, Ingredient::class], version = 3, exportSchema = false)
abstract class RecipesDatabase: RoomDatabase() {
    abstract fun recipesDao(): RecipesDao
    abstract fun ingredientsDao(): IngredientsDao
}