package com.axot.recipefinder.data.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.axot.recipefinder.data.entitis.Recipe
import com.axot.recipefinder.data.entitis.RecipeDetails

@Dao
interface RecipesDao: BaseDao<Recipe> {
    @Query("SELECT * FROM recipes")
    fun getAllRecipes(): LiveData<List<Recipe>>

    @Query("SELECT * FROM recipes WHERE id=:id")
    fun getRecipeDetails(id: Long): LiveData<RecipeDetails>

    @Query("SELECT * FROM recipes")
    fun getAllRecipeDetails(): LiveData<List<RecipeDetails>>
}