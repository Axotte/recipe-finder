package com.axot.recipefinder.dagger

import android.app.Application
import com.axot.recipefinder.RecipeApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, BuildersModule::class, AppModule::class])
interface AppComponent {
    fun inject(app: RecipeApplication)
}