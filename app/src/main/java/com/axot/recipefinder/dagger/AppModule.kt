package com.axot.recipefinder.dagger

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import com.axot.recipefinder.data.daos.IngredientsDao
import com.axot.recipefinder.data.daos.RecipesDao
import com.axot.recipefinder.data.database.RecipesDatabase
import com.axot.recipefinder.view.viewmodel.RecipesViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideRecipesDatabase(app: Application): RecipesDatabase = Room.databaseBuilder(app, RecipesDatabase::class.java, "RecipesDB")
        .fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    fun provideRecipesDao(recipesDatabase: RecipesDatabase): RecipesDao = recipesDatabase.recipesDao()

    @Provides
    @Singleton
    fun provideIngredientsDao(recipesDatabase: RecipesDatabase): IngredientsDao = recipesDatabase.ingredientsDao()

    @Provides
    fun provideRecipesViewModelFactory(factory: RecipesViewModelFactory): ViewModelProvider.Factory = factory
}