package com.axot.recipefinder.dagger

import com.axot.recipefinder.view.activities.AddEditRecipeActivity
import com.axot.recipefinder.view.activities.MainActivity
import com.axot.recipefinder.view.activities.RecipeDetailsActivity
import com.axot.recipefinder.view.activities.SearchActivity
import com.axot.recipefinder.view.fragments.SearchFragment
import com.axot.recipefinder.view.fragments.SearchResultFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
    @ContributesAndroidInjector
    abstract fun contributeRecipeDetailsActivity(): RecipeDetailsActivity
    @ContributesAndroidInjector
    abstract fun contributeAddEditRecipeActivity(): AddEditRecipeActivity
    @ContributesAndroidInjector
    abstract fun contributeSearchActivity(): SearchActivity
    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment
    @ContributesAndroidInjector
    abstract fun contributeSearchResultFragment(): SearchResultFragment
}