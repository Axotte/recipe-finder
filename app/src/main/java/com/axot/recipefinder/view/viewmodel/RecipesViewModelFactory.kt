package com.axot.recipefinder.view.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class RecipesViewModelFactory @Inject constructor(private val recipesViewModel: RecipesViewModel): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(RecipesViewModel::class.java)) {
            recipesViewModel as T
        } else {
            throw IllegalArgumentException("Unknown class name")
        }
    }
}