package com.axot.recipefinder.view.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.axot.recipefinder.data.Repository
import com.axot.recipefinder.data.entitis.Ingredient
import com.axot.recipefinder.data.entitis.Recipe
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class RecipesViewModel @Inject constructor(private val repository: Repository): ViewModel() {
    private val chosenIngrednients = MutableLiveData<List<String>>()
    fun setChosen(chosen: List<String>) {
        chosenIngrednients.value = chosen
    }
    fun getChosen(): LiveData<List<String>> = chosenIngrednients


    fun getAllRecipes() = repository.getAllRecipes()
    fun getRecipeDetails(id: Long) = repository.getRecipeDetails(id)
    fun getAllRecipeDetails() = repository.getAllRecipeDetails()
    fun getAllNames() = repository.getAllNames()
    fun insertFullRecipe(recipe: Recipe, vararg ingredient: Ingredient) {
        doAsync {
            val id = repository.insertRecipe(recipe)
            for (i in ingredient) {
                i.recipeId = id[0]
            }
            repository.insertIngredient(*ingredient)
        }
    }
    fun deleteRecipe(vararg recipe: Recipe) {
        doAsync {
            repository.deleteRecipe(*recipe)
        }
    }
    fun deleteIngredient(vararg ingredient: Ingredient) {
        doAsync {
            repository.deleteIngredient(*ingredient)
        }
    }
    fun deleteAllFromRecipe(recipeId: Long) {
        doAsync {
            repository.deleteAllFromRecipe(recipeId)
        }
    }
}