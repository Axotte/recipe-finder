package com.axot.recipefinder.view.adapters

import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.recipefinder.R
import com.axot.recipefinder.data.entitis.Recipe
import de.hdodenhof.circleimageview.CircleImageView
import java.io.File

class RecipesRvAdapter(val listener: (Long) -> Unit): RecyclerView.Adapter<RecipesRvAdapter.RecipesViewHolder>() {
    private var recipes: List<Recipe> = ArrayList()

    fun setRecipes(recipes: List<Recipe>) {
        this.recipes = recipes
        notifyDataSetChanged()
    }

    fun addRecipe(recipe: Recipe) {
        (recipes as ArrayList).add(recipe)
    }

    fun isEmpty(): Boolean = recipes.isEmpty()


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecipesViewHolder {
        val inflater = LayoutInflater.from(p0.context)
        return RecipesViewHolder(inflater.inflate(R.layout.recipe_rv_item, p0, false))
    }

    override fun getItemCount(): Int = recipes.size

    override fun onBindViewHolder(holder: RecipesViewHolder, position: Int) {
        holder.dishName.text = recipes[position].name
        if (recipes[position].image == "" && !File(recipes[position].image).exists()) {
            holder.dishImage.setBackgroundResource(R.mipmap.ic_launcher)
        } else {
            holder.dishImage.setImageBitmap(BitmapFactory.decodeFile(recipes[position].image))
        }
    }

    inner class RecipesViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val dishImage = itemView.findViewById<CircleImageView>(R.id.civ_dish_item)!!
        val dishName = itemView.findViewById<TextView>(R.id.tv_dish_name_item)!!
        init {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener(recipes[adapterPosition].id)
                }
            }
        }
    }
}