package com.axot.recipefinder.view.activities

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.animation.AnimationUtils
import com.axot.recipefinder.utils.Constants
import com.axot.recipefinder.R
import com.axot.recipefinder.view.adapters.RecipesRvAdapter
import com.axot.recipefinder.view.viewmodel.RecipesViewModel
import com.axot.recipefinder.view.viewmodel.RecipesViewModelFactory
import dagger.android.AndroidInjection
import org.jetbrains.anko.toast
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    //view model
    @Inject lateinit var recipesViewModelFactory: RecipesViewModelFactory
    private lateinit var recipesViewModel: RecipesViewModel
    //layout
    private lateinit var adapter: RecipesRvAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var fabAdd: FloatingActionButton
    private lateinit var fabSearch: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)
        getPermission()
        recipesViewModel = ViewModelProviders.of(this, recipesViewModelFactory).get(RecipesViewModel::class.java)
        initLayout()
    }

    private fun getPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    Constants.WRITE_STORAGE_PERMISSION_REQUEST_CODE)
            }
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    Constants.READ_STORAGE_PERMISSION_REQUEST_CODE)
            }
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                    Constants.CAMERA_PERMISSION_REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            Constants.WRITE_STORAGE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "Permission is granted")
                } else {
                    finish()
                }
            }
            Constants.CAMERA_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "Permission is granted")
                } else {
                    finish()
                }
            }
            Constants.READ_STORAGE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "Permission is granted")
                } else {
                    finish()
                }
            }
        }
    }

    private fun initLayout() {
        fabAdd = findViewById(R.id.fab_add)
        fabSearch = findViewById(R.id.fab_search)
        recyclerView = findViewById(R.id.rv_recipes)
        setupListeners()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recipesViewModel.getAllRecipes().observe(this, Observer {recipes ->
            recipes?.let {
                adapter.setRecipes(it)

                Handler().postDelayed({
                    recyclerView.scheduleLayoutAnimation()
                    recyclerView.invalidate()
                }, 10)

            }
        })
    }

    private fun setupListeners() {
        fabAdd.setOnClickListener {
            val intent = Intent(this, AddEditRecipeActivity::class.java).apply {
                putExtra(Constants.EXTRA_RECIPE_ID, -1L)
            }
            startActivity(intent)
        }
        fabSearch.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
        adapter = RecipesRvAdapter{
            val intent = Intent(this, RecipeDetailsActivity::class.java).apply {
                putExtra(Constants.EXTRA_RECIPE_ID, it)
            }
            startActivity(intent)
        }
    }
}
