package com.axot.recipefinder.view.activities

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import com.axot.recipefinder.R
import com.axot.recipefinder.view.fragments.SearchFragment
import com.axot.recipefinder.view.fragments.SearchResultFragment
import dagger.android.AndroidInjection

class SearchActivity : AppCompatActivity() {

    lateinit var fabShowResults: FloatingActionButton
    lateinit var fabBack: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        AndroidInjection.inject(this)
        val searchFragment = SearchFragment()

        fabShowResults = findViewById(R.id.fab_show_effects)
        fabBack = findViewById(R.id.fab_back)
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, searchFragment).commit()
        fabShowResults.setOnClickListener {
            searchFragment.sendChosen()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, SearchResultFragment())
                .addToBackStack("results")
                .commit()
        }
        fabBack.setOnClickListener {
            supportFragmentManager.popBackStackImmediate()
        }
    }
}
