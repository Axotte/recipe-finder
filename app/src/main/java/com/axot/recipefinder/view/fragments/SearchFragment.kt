package com.axot.recipefinder.view.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import com.axot.recipefinder.R
import com.axot.recipefinder.view.activities.SearchActivity
import com.axot.recipefinder.view.adapters.SearchIngredientsRvAdapter
import com.axot.recipefinder.view.viewmodel.RecipesViewModel
import com.axot.recipefinder.view.viewmodel.RecipesViewModelFactory
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchFragment : Fragment() {

    @Inject lateinit var viewModelFactory: RecipesViewModelFactory
    @Inject lateinit var adapterChosen: SearchIngredientsRvAdapter
    @Inject lateinit var adapterSearch: SearchIngredientsRvAdapter
    private var ingredientsNames = ArrayList<String>()
    private lateinit var viewModel: RecipesViewModel
    private lateinit var etSearch: EditText
    private lateinit var rvChosen: RecyclerView
    private lateinit var rvSearch: RecyclerView
    private lateinit var progressBar: ProgressBar

    private lateinit var disposable: Disposable

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        AndroidSupportInjection.inject(this)
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        etSearch = view.findViewById(R.id.et_search)
        rvChosen = view.findViewById(R.id.rv_chosen)
        rvSearch = view.findViewById(R.id.rv_search)
        progressBar = view.findViewById(R.id.progressBar)
        (activity as SearchActivity).fabShowResults.show()
        rvSearch.adapter = adapterSearch
        rvSearch.layoutManager = LinearLayoutManager(activity)
        rvChosen.adapter = adapterChosen
        rvChosen.layoutManager = LinearLayoutManager(activity)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as SearchActivity).fabShowResults.show()
        (activity as SearchActivity).fabBack.hide()
        setViewModel()
        setListeners()
        setObservables()
    }

    private fun setObservables() {

        disposable = createTextChangeObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { progressBar.visibility = View.VISIBLE }
            .observeOn(Schedulers.io())
            .map {query ->
                ingredientsNames.filter { it.toLowerCase().contains(query.toLowerCase()) }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                progressBar.visibility = View.GONE
                (it as ArrayList).removeAll(adapterChosen.getIngredientsNames())
                adapterSearch.setAllIngredients(it)
            }
    }

    private fun createTextChangeObservable(): Observable<String> {
        return  Observable.create<String> { emitter ->
            val textWatcher = object: TextWatcher {
                override fun afterTextChanged(s: Editable?) = Unit

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.toString()?.let { emitter.onNext(it) }
                }
            }
            etSearch.addTextChangedListener(textWatcher)

            emitter.setCancellable { etSearch.removeTextChangedListener(textWatcher) }
        }.debounce(500, TimeUnit.MILLISECONDS)
    }

    private fun setListeners() {
        adapterSearch.setOnItemClickListener {
            adapterChosen.addIngredient(adapterSearch.getNameFromPosition(it))
            adapterSearch.deleteIngredient(it)
        }
        adapterChosen.setOnItemClickListener {
            adapterSearch.addIngredient(adapterChosen.getNameFromPosition(it))
            adapterChosen.deleteIngredient(it)
        }
    }

    private fun setViewModel() {
        viewModel = ViewModelProviders.of(activity!! , viewModelFactory).get(RecipesViewModel::class.java)
        viewModel.getAllNames().observe(this, Observer { names ->
            names?.let {
                ingredientsNames = ArrayList(HashSet(it))
                adapterSearch.setAllIngredients(ingredientsNames)
                Handler().postDelayed({
                    rvSearch.scheduleLayoutAnimation()
                    rvSearch.invalidate()
                },10)
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (!disposable.isDisposed) disposable.dispose()
    }

    fun sendChosen() {
        viewModel.setChosen(adapterChosen.getIngredientsNames())
    }

}
