package com.axot.recipefinder.view.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.axot.recipefinder.R
import com.axot.recipefinder.data.entitis.Recipe
import com.axot.recipefinder.utils.Constants
import com.axot.recipefinder.view.adapters.IngredientsRvAdapter
import com.axot.recipefinder.view.viewmodel.RecipesViewModel
import com.axot.recipefinder.view.viewmodel.RecipesViewModelFactory
import dagger.android.AndroidInjection
import de.hdodenhof.circleimageview.CircleImageView
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton
import java.io.File
import javax.inject.Inject

class RecipeDetailsActivity : AppCompatActivity() {
    private var recipeId: Long = -1L
    //view
    @Inject lateinit var adapter: IngredientsRvAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var image: CircleImageView
    private lateinit var name: TextView
    private lateinit var description: TextView
    private lateinit var fabDelete: FloatingActionButton
    private lateinit var fabEdit: FloatingActionButton
    //view model
    @Inject lateinit var recipesViewModelFactory: RecipesViewModelFactory
    private lateinit var recipesViewModel: RecipesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_details)
        AndroidInjection.inject(this)
        recipesViewModel = ViewModelProviders.of(this, recipesViewModelFactory).get(RecipesViewModel::class.java)
        recipeId = intent.getLongExtra(Constants.EXTRA_RECIPE_ID, -1L)
        initLayout()
    }

    private fun initLayout() {
        name = findViewById(R.id.tv_recipe_name)
        image = findViewById(R.id.civ_dish)
        recyclerView = findViewById(R.id.rv_ingredients)
        description = findViewById(R.id.tv_description)
        fabDelete = findViewById(R.id.fab_delete_recipe)
        fabEdit = findViewById(R.id.fab_edit)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        if (recipeId != -1L) {
            recipesViewModel.getRecipeDetails(recipeId).observe(this, Observer { recipe ->
                recipe?.let {
                    name.text = it.recipe!!.name
                    description.text = it.recipe!!.description
                    if (it.recipe!!.image != "" && File(it.recipe!!.image).exists()) {
                        image.setImageBitmap(BitmapFactory.decodeFile(it.recipe!!.image))
                    }
                    adapter.setIngredients(it.ingredients)
                    Handler().postDelayed({
                        recyclerView.scheduleLayoutAnimation()
                        recyclerView.invalidate()
                    }, 10)
                }
            })
        } else {
            finish()
        }
        fabDelete.setOnClickListener {
            alert("Na pewno usunąć przepis?") {
                yesButton {
                    recipesViewModel.deleteRecipe(Recipe(id = recipeId))
                    finish()
                }
                noButton {  }
            }.show()
        }
        fabEdit.setOnClickListener {
            val intent = Intent(this, AddEditRecipeActivity::class.java).apply {
                putExtra(Constants.EXTRA_RECIPE_ID, recipeId)
            }
            startActivity(intent)
        }
    }
}
