package com.axot.recipefinder.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.recipefinder.R
import com.axot.recipefinder.data.entitis.Ingredient
import javax.inject.Inject

class IngredientsRvAdapter @Inject constructor(): RecyclerView.Adapter<IngredientsRvAdapter.IngredientsViewHolder>() {
    private var ingredients: List<Ingredient> = ArrayList()

    fun setIngredients(ingredients: List<Ingredient>) {
        this.ingredients = ingredients
        notifyDataSetChanged()
    }
    fun getIngredients() = ingredients

    fun deleteIngredientAtPosition(position: Int) {
        (ingredients as ArrayList).removeAt(position)
        notifyItemRemoved(position)
    }
    fun addIngredient(ingredient: Ingredient) {
        (ingredients as ArrayList).add(ingredient)
        notifyItemInserted(ingredients.size - 1)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): IngredientsViewHolder {
        val inflater = LayoutInflater.from(p0.context)
        return IngredientsViewHolder(inflater.inflate(R.layout.ingredient_rv_item, p0, false))
    }

    override fun getItemCount(): Int = ingredients.size

    override fun onBindViewHolder(holder: IngredientsViewHolder, position: Int) {
        holder.ingredientName.text = ingredients[position].name
        val string = if (ingredients[position].unit == "---") {
            "---"
        } else {
            val quantity = String.format("%.1f", ingredients[position].quantity)
            "$quantity ${ingredients[position].unit}"
        }
        holder.ingredientQuantity.text = string
    }

    inner class IngredientsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ingredientName = itemView.findViewById<TextView>(R.id.tv_ingredient_name_item)!!
        val ingredientQuantity = itemView.findViewById<TextView>(R.id.tv_ingredient_quantity_item)!!
    }
}