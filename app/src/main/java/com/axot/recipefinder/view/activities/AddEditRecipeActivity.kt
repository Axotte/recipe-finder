package com.axot.recipefinder.view.activities

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.*
import com.axot.recipefinder.R
import com.axot.recipefinder.data.entitis.Ingredient
import com.axot.recipefinder.data.entitis.Recipe
import com.axot.recipefinder.utils.Constants
import com.axot.recipefinder.utils.SwipeToDeleteCallback
import com.axot.recipefinder.view.adapters.IngredientsRvAdapter
import com.axot.recipefinder.view.viewmodel.RecipesViewModel
import com.axot.recipefinder.view.viewmodel.RecipesViewModelFactory
import dagger.android.AndroidInjection
import de.hdodenhof.circleimageview.CircleImageView
import org.jetbrains.anko.toast
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class AddEditRecipeActivity : AppCompatActivity() {
    private var recipeId: Long = -1L
    private var imagePath = ""
    private lateinit var ingredientsNames: List<String>

    //view model
    @Inject lateinit var recipesViewModelFactory: RecipesViewModelFactory
    private lateinit var recipesViewModel: RecipesViewModel
    //view
    private lateinit var civImage: CircleImageView
    private lateinit var etRecipeName: EditText
    private lateinit var actvIngredientName: AutoCompleteTextView
    private lateinit var etIngredientQuantity: EditText
    private lateinit var spinnerIngredientUnit: Spinner
    private lateinit var etDescription: EditText
    private lateinit var recyclerView: RecyclerView
    private lateinit var fabSave: FloatingActionButton
    private lateinit var fabMoreOptions: FloatingActionButton
    private lateinit var fabCloseOptions: FloatingActionButton
    private lateinit var fabPhoto: FloatingActionButton
    private lateinit var fabPhotoCamera: FloatingActionButton
    private lateinit var butAddIngredient: Button
    @Inject lateinit var adapter: IngredientsRvAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_recipe)
        AndroidInjection.inject(this)
        recipesViewModel = ViewModelProviders.of(this, recipesViewModelFactory).get(RecipesViewModel::class.java)
        initLayout()
        setupListeners()
        recipeId = intent.getLongExtra(Constants.EXTRA_RECIPE_ID, -1L)
        if (recipeId != -1L) fillInformations()
    }

    private fun fillInformations() {
        recipesViewModel.getRecipeDetails(recipeId).observe(this, Observer {recipeDetails ->
            recipeDetails?.let {
                etRecipeName.setText(it.recipe!!.name)
                etDescription.setText(it.recipe!!.description)
                if (it.recipe!!.image != "" && File(it.recipe!!.image).exists()) {
                    imagePath = it.recipe!!.image
                    civImage.setImageBitmap(BitmapFactory.decodeFile(imagePath))
                }
                adapter.setIngredients(it.ingredients)
            }
        })
    }

    private fun setupListeners() {
        spinnerIngredientUnit.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                etIngredientQuantity.isEnabled = true
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val item = parent!!.getItemAtPosition(position) as String
                etIngredientQuantity.isEnabled = item != "---"
                if (item == "---") etIngredientQuantity.setText("")
            }
        }
        val swipeHandler = object: SwipeToDeleteCallback(this) {
            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                adapter.deleteIngredientAtPosition(p0.adapterPosition)
            }
        }
        ItemTouchHelper(swipeHandler).attachToRecyclerView(recyclerView)

        fabMoreOptions.setOnClickListener {
            showMenu()
        }
        fabCloseOptions.setOnClickListener {
            hideMenu()
        }
        civImage.setOnClickListener {
            showMenu()
        }
        fabPhotoCamera.setOnClickListener {
            takePhoto()
        }
        fabPhoto.setOnClickListener {
            pickFromGallery()
        }
        fabSave.setOnClickListener {
            insertToDatabase()
            finish()
        }
        butAddIngredient.setOnClickListener {
            addIngredient()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale("pl", "PL")).format(Date())
        val storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "recipefinder").apply {
            if (!exists()) mkdir()
        }
        return File.createTempFile("JPEG_${timeStamp}_", ".jpg", storageDir).apply {
            imagePath = absolutePath
        }
    }

    private fun takePhoto() {
        try {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
                putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this@AddEditRecipeActivity,
                    "com.axot.provider",createImageFile()))
            }
            startActivityForResult(intent, Constants.TAKE_PHOTO)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun pickFromGallery() {
        val intent = Intent(Intent.ACTION_PICK).apply {
            type = "image/"
        }
        startActivityForResult(intent, Constants.PICK_PHOTO)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when(requestCode) {
                Constants.TAKE_PHOTO -> {
                    civImage.setImageBitmap(BitmapFactory.decodeFile(imagePath))
                }
                Constants.PICK_PHOTO -> {
                    val selectedImage = data!!.data!!
                    imagePath = getPathFromUri(selectedImage)
                    civImage.setImageBitmap(BitmapFactory.decodeFile(imagePath))
                }
            }
        }
    }

    private fun getPathFromUri(uri: Uri): String {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(uri, filePathColumn, null, null, null)
        var path = ""
        cursor?.let {
            it.apply {
                moveToFirst()
                path = getString(getColumnIndex(filePathColumn[0]))
            }
        }
        cursor?.close()
        return path
    }

    private fun insertToDatabase() {
        val name = etRecipeName.text.toString()
        val description = etDescription.text.toString()
        if (name == "") {
            toast("wpisz nazwę")
            return
        }
        val recipe: Recipe = if (recipeId != -1L) {
            recipesViewModel.deleteAllFromRecipe(recipeId)
            Recipe(id = recipeId, name = name, image = imagePath, description = description)
        } else {
            Recipe(name = name, image = imagePath, description = description)
        }
        recipesViewModel.insertFullRecipe(recipe, *adapter.getIngredients().toTypedArray())
    }

    private fun addIngredient() {
        val name = actvIngredientName.text.toString()
        val unit = spinnerIngredientUnit.selectedItem as String
        val quantity = etIngredientQuantity.text.toString().toDoubleOrNull()
        if (name != "" && unit != "" && quantity != null) {
            adapter.addIngredient(Ingredient(name = name, unit = unit, quantity = quantity))
            actvIngredientName.setText("")
            etIngredientQuantity.setText("")
        } else if(name != "" && unit == "---") {
            adapter.addIngredient(Ingredient(name = name, unit = unit))
            actvIngredientName.setText("")
            etIngredientQuantity.setText("")
        }else {
            toast("Wypełnij wszystkie pola")
        }
    }

    private fun initLayout() {
        civImage = findViewById(R.id.civ_dish_add_edit)
        etRecipeName = findViewById(R.id.et_recipe_name)
        actvIngredientName = findViewById(R.id.actv_ingredient_name)
        etIngredientQuantity = findViewById(R.id.et_ingredient_quantity)
        spinnerIngredientUnit = findViewById(R.id.spinner_unit)
        etDescription = findViewById(R.id.et_description)
        recyclerView = findViewById(R.id.rv_ingredients_add_edit)
        fabSave = findViewById(R.id.fab_save)
        fabMoreOptions = findViewById(R.id.fab_more_options)
        fabCloseOptions = findViewById(R.id.fab_close_options)
        fabPhoto = findViewById(R.id.fab_photo)
        fabPhotoCamera = findViewById(R.id.fab_photo_camera)
        butAddIngredient = findViewById(R.id.but_add_ingredient)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = ArrayAdapter.createFromResource(this, R.array.units, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerIngredientUnit.adapter = adapter
        recipesViewModel.getAllNames().observe(this, Observer { names ->
            names?.let {
                val removeDuplicates = ArrayList<String>(HashSet<String>(it))
                val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, removeDuplicates)
                actvIngredientName.setAdapter(arrayAdapter)
            }
        })
    }

    private fun hideMenu() {
        val handler = Handler()
        fabCloseOptions.hide()
        fabMoreOptions.show()
        handler.postDelayed({
            fabPhotoCamera.hide()
        }, 50)
        handler.postDelayed({
            fabPhoto.hide()
        }, 100)


    }
    private fun showMenu() {
        val handler = Handler()
        fabCloseOptions.show()
        fabMoreOptions.hide()
        handler.postDelayed({
            fabPhoto.show()
        }, 50)
        handler.postDelayed({
            fabPhotoCamera.show()
        }, 100)
    }
}
