package com.axot.recipefinder.view.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.axot.recipefinder.R
import com.axot.recipefinder.data.entitis.RecipeDetails
import com.axot.recipefinder.utils.Constants
import com.axot.recipefinder.view.activities.RecipeDetailsActivity
import com.axot.recipefinder.view.activities.SearchActivity
import com.axot.recipefinder.view.adapters.RecipesRvAdapter
import com.axot.recipefinder.view.viewmodel.RecipesViewModel
import com.axot.recipefinder.view.viewmodel.RecipesViewModelFactory
import dagger.android.support.AndroidSupportInjection
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import javax.inject.Inject

class SearchResultFragment : Fragment() {

    private lateinit var viewModel: RecipesViewModel
    @Inject lateinit var viewModelFactory: RecipesViewModelFactory
    private val textViews: ArrayList<TextView> = ArrayList()
    private val recyclerViews: ArrayList<RecyclerView> = ArrayList()
    private val adapters: ArrayList<RecipesRvAdapter> = ArrayList()
    private lateinit var progressBar: ProgressBar
    private lateinit var allRecipeDetails: List<RecipeDetails>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_search_result, container, false)
        AndroidSupportInjection.inject(this)

        progressBar = view.findViewById(R.id.pb_results)

        textViews.apply {
            add(view.findViewById(R.id.tv_0))
            add(view.findViewById(R.id.tv_1))
            add(view.findViewById(R.id.tv_2))
            add(view.findViewById(R.id.tv_3))
            add(view.findViewById(R.id.tv_4))
            add(view.findViewById(R.id.tv_5))
            add(view.findViewById(R.id.tv_more))
        }
        recyclerViews.apply {
            add(view.findViewById(R.id.rv_0))
            add(view.findViewById(R.id.rv_1))
            add(view.findViewById(R.id.rv_2))
            add(view.findViewById(R.id.rv_3))
            add(view.findViewById(R.id.rv_4))
            add(view.findViewById(R.id.rv_5))
            add(view.findViewById(R.id.rv_more))
        }

        (activity as SearchActivity).fabShowResults.hide()
        (activity as SearchActivity).fabBack.show()
        configRecyclerViews()

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(RecipesViewModel::class.java)
        viewModel.getChosen().observe(this, Observer { c ->
            c?.let {chosen ->
                viewModel.getAllRecipeDetails().observe(this, Observer { details ->
                    details?.let {
                        allRecipeDetails = it
                        doAsync {
                            compareIngredients(chosen)
                            uiThread {
                                showData()
                            }
                        }
                    }
                })
            }
        })
    }

    private fun showData() {
        progressBar.visibility = View.GONE
        for (i in allRecipeDetails) {
            when(i.ingredients.size) {
                0 -> {
                    adapters[0].addRecipe(i.recipe!!)
                }
                1 -> {
                    adapters[1].addRecipe(i.recipe!!)
                }
                2 -> {
                    adapters[2].addRecipe(i.recipe!!)
                }
                3 -> {
                    adapters[3].addRecipe(i.recipe!!)
                }
                4 -> {
                    adapters[4].addRecipe(i.recipe!!)
                }
                5 -> {
                    adapters[5].addRecipe(i.recipe!!)
                }
                else -> {
                    adapters[6].addRecipe(i.recipe!!)
                }
            }
        }
        for(i in 0..6) {
            if (!adapters[i].isEmpty()) {
                textViews[i].visibility = View.VISIBLE
                recyclerViews[i].visibility = View.VISIBLE
            }
        }
        recyclerViews.forEach {
            it.visibility = View.VISIBLE
            val adapter = (it.adapter!! as RecipesRvAdapter)
            if (!adapter.isEmpty()) {
                adapter.notifyDataSetChanged()
                Handler().postDelayed({
                    it.scheduleLayoutAnimation()
                    it.invalidate()
                }, 10)
            }
        }
    }

    private fun compareIngredients(chosen: List<String>) {
        for (i in chosen) {
            allRecipeDetails.forEach {recipeDetails ->
                recipeDetails.ingredients.filter {
                    it.name.toLowerCase() == i.toLowerCase()
                }.forEach { (recipeDetails.ingredients as ArrayList).remove(it) }
            }
        }
    }

    private fun configRecyclerViews() {
        adapters.apply {
            for (i in 0..6) {
                add(RecipesRvAdapter {
                    val intent = Intent(activity, RecipeDetailsActivity::class.java).apply {
                        putExtra(Constants.EXTRA_RECIPE_ID, it)
                    }
                    startActivity(intent)
                })
            }
        }

        for (i in 0..6) {
            recyclerViews[i].apply {
                adapter = adapters[i]
                layoutManager = LinearLayoutManager(this@SearchResultFragment.activity)
            }
        }
    }
}
