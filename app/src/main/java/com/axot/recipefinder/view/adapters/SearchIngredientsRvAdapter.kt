package com.axot.recipefinder.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.axot.recipefinder.R
import javax.inject.Inject

class SearchIngredientsRvAdapter @Inject constructor(): RecyclerView.Adapter<SearchIngredientsRvAdapter.ViewHolder>() {
    private var ingredientsNames: List<String> = ArrayList()
    private var listener: ((Int) -> Unit) ?= null

    fun setOnItemClickListener(listener: (Int) -> Unit) {
        this.listener = listener
    }

    fun getIngredientsNames() = ingredientsNames

    fun getNameFromPosition(position: Int) = ingredientsNames[position]

    fun setAllIngredients(ingredientsNames: List<String>) {
        this.ingredientsNames = ingredientsNames
        notifyDataSetChanged()
    }

    fun addIngredient(ingredientsName: String) {
        (ingredientsNames as ArrayList).add(ingredientsName)
        notifyItemInserted(ingredientsNames.size - 1)
    }
    
    fun deleteIngredient(ingredientPosition: Int) {
        (ingredientsNames as ArrayList).removeAt(ingredientPosition)
        notifyItemRemoved(ingredientPosition)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.search_item, p0, false))
    }

    override fun getItemCount(): Int = ingredientsNames.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.name.text = ingredientsNames[p1]
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.tv_item_search_name)
        init {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener?.invoke(adapterPosition)
                }
            }
        }
    }
}