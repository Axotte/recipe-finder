package com.axot.recipefinder

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.axot.recipefinder.dagger.AppModule
import com.axot.recipefinder.dagger.DaggerAppComponent
import dagger.android.*
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class RecipeApplication: Application(), HasActivityInjector, HasSupportFragmentInjector{

    @Inject lateinit var activityInjector: DispatchingAndroidInjector<Activity>
    @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().appModule(AppModule(this)).build().inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

}