package com.axot.recipefinder.utils

class Constants {
    companion object {
        const val EXTRA_RECIPE_ID = "com.example.recipefinder.EXTRA_RECIPE_ID"
        const val TAKE_PHOTO = 0
        const val PICK_PHOTO = 1
        const val WRITE_STORAGE_PERMISSION_REQUEST_CODE = 2137
        const val CAMERA_PERMISSION_REQUEST_CODE = 2115
        const val READ_STORAGE_PERMISSION_REQUEST_CODE = 1111
    }
}